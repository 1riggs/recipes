# Chicken Stock

[Source](https://rouxbe.com/recipes/850-basic-white-chicken-stock)

This basic white chicken stock is easy to make. The liquid from slowly simmering chicken bones, vegetables, herbs and spices will add incredible flavor to your cooking.

-   **Serves:** 6 L
-   **Active Time:** 25 mins
-   **Total Time:** 3 hrs

## Ingredients

- 6 lb chicken bones (backs and necks)  
- cold water  
- 3 ribs celery  
- 3 carrots  
- 3 onions  
- 2 leeks  
- 2 tsp whole, black peppercorns  
- 2 bay leaves  
- 10 to 12 stems fresh parsley  
- 3 to 4 stems fresh thyme  
- small bunch of celery leaves

## Recipe

### Step 1 Making and Cooking the Stock

To start the stock, rinse the bones under cold water and place them into a suitable-sized stock pot. Cover the bones with cold water by about 2 inches. Turn the heat to medium and slowly bring the bones to a simmer, making sure it doesn’t come to a boil.

In the meantime, chop the mirepoix (onions, leeks, celery and carrots) into about 1/2" to 3/4" -inch pieces.

After the stock has simmered for about 30 minutes, skim one more time before adding the mirepoix.

Let the stock gently simmer for another hour or so, skimming the surface as needed.

Then add the bouquet garni (peppercorns, bay leaves, parsley stems, fresh thyme and celery leaves), making sure to gently tuck it underneath the surface. Continue to simmer for about 30 minutes.

### Step 2 Finishing the Stock

Once the stock has cooked for at least 1 1/2 to 2 hours, you can strain it. First, skim off as much fat as possible from the surface. Then gently remove the solids and discard. Finally, strain the stock through a sieve lined with a piece of cheesecloth.

You can either use the stock immediately or cool it over an ice bath. Once cool, it can be stored in the refrigerator for a few days or it can be portioned and frozen for several months.


