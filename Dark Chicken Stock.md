# Dark Chicken Stock
[Source](https://rouxbe.com/recipes/800-dark-chicken-stock)

A rich, dark chicken stock with loads of flavor. It's a practical and delicious alternative to veal stock.

-   **Serves:** About 4 liters
-   **Active Time:** 45 mins
-   **Total Time:** 7 hrs - 8 hrs

## Ingredients

* 6 lb chicken
* 3 large carrots
* 3 large onions
* 3 celery ribs
* 1 leek
* 2 heads of garlic
* 3 tbsp tomato paste
* 3/4c dry white wine
* 6 tbsp vegetable oil
* 2 bay leaves  
* 5 to 6 sprigs fresh thyme  
* 1/2 bunch parsley stems  
* 1 1/2 tsp whole, black peppercorns
## Recipe

#### Step 1: Caramelizing the Bones and Mirepoix
Preheat the oven to 425º degrees Fahrenheit. Lightly coat a roasting pan with 2 tablespoons of the oil and lay the bones in a single layer. Place into the oven to caramelize.

Meanwhile, wash and roughly chop all of the mirepoix into approximately 3/4" to 1" -inch pieces. Keep the leeks separate, as they will be added a bit later. Cut the garlic in half horizontally.

For easy clean up, line a baking tray with foil all the way up the sides. Add about 1 tablespoon of the oil and the mirepoix. Add another tablespoon of oil over top and toss to coat. Place into the oven for about 30 minutes.

Check the bones after about 40 minutes or so. If they’re golden brown, turn them over and place them back into the oven to caramelize on the other side. Toss the vegetables occasionally to ensure they’re cooking evenly.

Now, add the leeks, toss again and place back into the oven. Once the mirepoix is done, push it towards the center of the foil, being careful not to tear it.

Next, heat a stock pot to medium and add the rest of the oil (about 2 tablespoons). Transfer the mirepoix to the pot and add the tomato paste. Cook for about a minute or so, turn off the heat and set aside while you check on the bones. Once the bones are nicely caramelized, add them to the stock pot.

Carefully drain the excess fat from the roasting pan. Place the pan onto the stove top and turn the heat to medium-high. Deglaze with the white wine. Once the wine has reduced, scrape the bottom and pour everything into the stock pot.
#### Step 2: Cooking the Stock
To cook the stock, cover the bones and mirepoix with enough cold water to cover everything by about 2 inches. Slowly bring the stock to a simmer over medium heat.

As the stock heats up, skim the surface periodically to remove any fat and impurities. Cook the stock for approximately 4 to 6 hours.

About 30 minutes before the stock has finished cooking, add the bouquet garni and continue to simmer.
#### Step 3: Finishing the Stock
Once the stock has finished cooking, gently remove the bones and mirepoix using a spider. Strain the stock through a sieve lined with cheesecloth.

You now have a beautiful rich chicken stock, which can be used in soups, stews and many other dishes.

If not using immediately, cool the stock over an ice bath and store appropriately.
#### Step 4: Dark Chicken Stock Reduction
You can further reduce this stock to create an even richer stock with a sauce-like consistency.

Simply place it back on the stove and simmer until the stock is reduced by half.

The reduced dark chicken stock can be used as the base for many healthy and delicious dishes and sauces.


