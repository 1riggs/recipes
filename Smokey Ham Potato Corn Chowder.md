
# Smokey Ham, Potato, and Corn Chowder

 Serves ~4 comfortably
## Ingredients
-   3 thick cut slices of bacon (chopped)
-   1 ham steak (cubed) (or use pork belly)
-   1 medium yellow onion (chopped fine)
-   2 garlic cloves (minced)
-   about 6 large red skin potatoes (diced into bite sized pieces)
-   (3) 10 oz. cans of sweet corn (drained) (brown this before adding to soup)
-   8 oz. original cream cheese (room temp)
-   1 can condensed cream of celery soup
-   1/2 cup white wine
-   4 cups water OR 4 cups chicken/pork stock
-   2 tablespoons Herbs de Provence (dry spice)
-   2-3 chicken bouillon cubes
-   3 bay leaves
-   salt and pepper to taste
-   about 2 tablespoons of butter
    

Proposed change: Use lightly roasted potatoes or crispy potatoes like American fries
# Recipe
In a large cast iron soup pot, melt about two tablespoons butter on medium-high heat. If using pork belly, start browning that first, it takes longer than the bacon. Add in you chopped bacon and render the fat from the bacon, stirring often with a wooden spoon.
    
Once the bacon looks cooked through and crispy, and you have a good amount of bacon fat in your pot, add in the white wine to scrape up the bits from the bottom of the pot. Add your chopped onion and garlic, and cook for 2-3 minutes. Stir often.
    
Now add in your ham and corn and cook for another 2-3 minutes. Stir often.
    
Add in your chopped potatoes, cream cheese, cream of celery soup, and give everything a stir.
    
Lower heat to low setting, add in your Herb de Provence, bay leaves, chicken bouillon cubes, about a teaspoon of salt, and fresh ground pepper, and 4 cups of water.
    
Cover and cook for about an hour on low (**about halfway through, remove lid and taste for seasonings. Since the bacon and ham are salty we don’t want to add too much salt, but you may need more, so taste and adjust salt and pepper if needed)
